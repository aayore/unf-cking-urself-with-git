#!/bin/bash

# Scenario:
# You made a commit with the wrong title or
# commit message.


demodir=demo/amend
feature=a

mkdir -p ${demodir}

echo "Hello world" > ${demodir}/feature_${feature}

git add ${demodir}/feature_${feature}

git commit -m "send dudes"


# Fix:
# git commit --amend

# NOTE: This rewrites history and may require
# a rebase if already published.  As a general
# rule, only do this immediately after making
# the commit.