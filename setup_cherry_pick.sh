#!/bin/bash

# Scenario:
# You and your teammate have been working in
# parallel, but your teammate made an update
# that you now need in your branch.  That change
# hasn't been merged back to main yet.


git checkout -b something_important

./make_commits.sh important a b

git checkout -b has_dependency main

./make_commits.sh important c d


# Fix:
# git cherry-pick <sha>