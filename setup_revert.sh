#! /bin/bash

# Scenario:
# You created a commit a while ago, and you want
# to undo it.  For example, imagine you changed
# `debug` from `false` to `true`, and you now
# want to change it back.


./make_commits.sh revert a undo c d

git --no-pager log


# Fix:
# git commit revert <sha>