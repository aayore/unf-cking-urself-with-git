# unf-cking-urself-with-git

![QR Code](class_qr_link.png)

---

## Automated Undo: `git revert`

Imagine you created a commit a while ago, and you want to undo it.  For example, imagine you changed `debug` from `false` to `true`, and you now want to change it back.

```shell
git revert <SHA>
```

---

## Roll Everything Back: `git reset`

You have several recent commits you want to trash.

```shell
git reset <SHA>
```

<font color="red">DANGER</font>

---

## New Branch from Staged Work: `git stash` and `git stash pop`

Sometimes you might start working on something before you realize it will be better off on another branch.

```shell
# Stage the work you want to migrate
git add (-A, list of files, etc.)

# Store/hide uncommitted work
git stash

# Create a new branch from the current branch/commit
git checkout -b <new_branch_name>
# Or from main
git checkout -b <new_branch_name> main

# Restore your stashed work
git stash pop
```

---

## A Commit From an Alternate Universe: `git cherry-pick`

You and your teammate have been working in parallel, but your teammate made an update that you now need in your branch.  That change hasn't been merged back to main yet.

```shell
git cherry-pick <SHA>
```

Note: This will append the commit to the HEAD of your current branch.  No attempt is made to determine chronology.

Example: I made a change to a `.gitlab-ci.yml` file in my branch; my coworker needed that specific change in his branch.

---

## Mulligan: `git commit --amend`

Used your personal email in your professional commit?  Typo'd the title?

```shell
git commit --amend
```

<font color="red">DANGER</font>

---

## Split the Commit: `git add -i`
 
Sometimes you'll get on a roll and make way too many changes in a single file.  This allows you to step through a file and select certain lines.

```shell
git add -i
```

---

## Half-Cached: `git diff --cached`

`git diff` is a well-known command that compares edited files against the latest commit.  But it has a pretty specific behavior you might not expect: the comparison prefers _staged files_ over the latest commit.

```shell
# Compare unstaged, edited files against staged files
# Or against HEAD if there are no staged files
git diff

# Compare unstaged, edited files against HEAD, explicitly
git diff HEAD

# Compare staged files against HEAD
git diff --cached
```

---

## Per-Character Diff: `--word-diff-regex=.`

This is actually really handy to do outside of the usual `git` context.  In any situation where you need a character-by-character diff between files.  But of course it works in a git repo, too.

```shell
# Compare an edited file against the last commit (or staged) version
git diff --word-diff-regex=.

# Compare two local files (no git repo necessary)
git diff --word-diff-regex=. --no-index <file1> <file2>
```
