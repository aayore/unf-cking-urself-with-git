#! /bin/bash

# Scenario:
# You've been assigned two features.  You have
# made progress on one, and start on the other.
# But you forgot to create a new/different
# branch.

./make_commits.sh stash a b c d

echo "Hello mars" > demo/stash/newfile

echo

git --no-pager log

echo

git status


# Fix:
# git stash
# git checkout -b new_branch
# git stash pop