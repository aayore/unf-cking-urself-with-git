#!/bin/bash

demodir=demo/cache_diff

mkdir -p ${demodir}


# Commit a change in demofile

echo commit > ${demodir}/demofile

echo FILE AS COMMITTED:

echo ""

cat ${demodir}/demofile

echo ""

git add ${demodir}/demofile

git commit -m 'committed'


# Stage a change in demofile

echo stage >> ${demodir}/demofile

echo FILE AS STAGED:

echo ""

cat ${demodir}/demofile

echo ""

git add ${demodir}/demofile


# Save an edit in demofile

echo edit >> ${demodir}/demofile

echo FILE CURRENT EDIT:

echo ""

cat ${demodir}/demofile

echo ""
