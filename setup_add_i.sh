#!/bin/bash

# Scenario:
# As part of troubleshooting, you updated a
# config file in multiple areas.  You did not
# create smaller commits for different changes.

demodir=demo/interactive

mkdir -p ${demodir}

cat << EOF > ${demodir}/config
[Network Settings]
Fast=false
Latency=high

[CPU Settings]
Threads=1
EOF

git add ${demodir}/config

git commit -m 'baseline config'

cat << EOF > ${demodir}/config
[Network Settings]
Fast=true
Latency=low

[CPU Settings]
Threads=32
EOF
