#!/bin/bash

demodir=demo/$1

mkdir -p ${demodir}

shift 1

for feature in "$@"; do 

    echo "Hello world" > ${demodir}/feature_${feature}

    git add ${demodir}/feature_${feature}

    git commit -m "implements feature_${feature}"

done
