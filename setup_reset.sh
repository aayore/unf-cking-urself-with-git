#! /bin/bash

# Scenario:
# You have several recent commits you want to
# trash.


./make_commits.sh reset a b mistake1 mistake2

git --no-pager log


# Fix:
# git reset <sha>

# NOTE: This rewrites history and may require
# a rebase if already published.  As a general
# rule, only do this if the commits you're 
# trashing only exist locally.